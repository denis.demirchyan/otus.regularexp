﻿#nullable enable
using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Otus.RegularExp
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            Console.WriteLine("Enter url:");
            var url = Console.ReadLine() ?? "";
            await DownloadImagesFromUrl(url);
        }

        /// <summary>
        /// Загрузка изображения.
        /// </summary>
        /// <param name="urlImage">Url изображения.</param>
        /// <param name="nameImage">Имя изображения для сохранения.</param>
        /// <param name="baseUrl">Url для изображений с неабсолютным путем.</param>
        private static async Task DownloadImage(string urlImage, string? nameImage = null, string? baseUrl = null)
        {
            nameImage ??= urlImage.Split('/').Last();

            try
            {
                Console.WriteLine($"Try download {nameImage}..");
                urlImage = PrepareUrlImage(urlImage, baseUrl?.Split('/').First());
                using var web = new WebClient();
                await web.DownloadFileTaskAsync(new Uri(urlImage), $"{Environment.CurrentDirectory}/images/{nameImage}");
                Console.WriteLine($"Image {nameImage} was downloaded.");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: Image: {nameImage} not download. Url: {urlImage}. Exception: {e.Message}");
            }            
        }

        /// <summary>
        /// Загрузка всех изображений со страницы.
        /// </summary>
        /// <param name="url">Url страницы</param>
        private static async Task DownloadImagesFromUrl(string url)
        {
            string page = await GetHtml(url);

            var regex = new Regex(@"<img\s.*?src=(?:'|"")([^'"">]+)(?:'|"")");
            var matches = regex.Matches(page);
            if (matches.Count <= 0)
            {
                Console.WriteLine("Not found images.");
            }
            else
            {
                Console.WriteLine($"Download {matches.Count} images..");
                foreach (Match match in matches)
                {
                    var urlImage = match.Groups[1].Value;
                    await DownloadImage(urlImage, baseUrl: url);
                }

                Console.WriteLine("Download completely.");
            }
        }
        
        /// <summary>
        /// Подготовка пути к изображению.
        /// </summary>
        /// <param name="urlImage">Url изображения.</param>
        /// <param name="baseUrl">Url сайта.</param>
        /// <returns>Подготовленный путь.</returns>
        private static string PrepareUrlImage(string urlImage, string? baseUrl=null)
        {
            if (urlImage.StartsWith("//"))
            {
                urlImage = urlImage[2..];
            }

            if (urlImage.StartsWith("/"))
            {
                if (baseUrl != null)
                    urlImage = new Uri(new Uri(baseUrl), urlImage).ToString();
            }
            
            if (!urlImage.StartsWith("http"))
            {
                urlImage = "https://" + urlImage;
            }

            return urlImage;
        }
        
        /// <summary>
        /// Получить страницу по url.
        /// </summary>
        /// <param name="url">Url страницы.</param>
        /// <returns>Контент страницы.</returns>
        private static async Task<string> GetHtml(string url)
        {
            try
            {
                using var client = new WebClient();
                return await client.DownloadStringTaskAsync(new Uri(url));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Can not get page by url: {url}. Exception: {e.Message}");
                throw;
            }
        }
    }
}